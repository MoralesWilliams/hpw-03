datox =[["c","c"],["d","d"]];
titulox = ["numero","a","b"];

function crear_tabla(titulo,dato) {
  var body = document.getElementsByTagName("body")[0];
  var tabla   = document.createElement("table");
  var tdhead = document.createElement("thead");
  var tblBody = document.createElement("tbody");
  var hilera=document.createElement("tr");
  for (var i = 0; i < titulo.length; i++) {
      var celda = document.createElement("th");
      var textoCelda = document.createTextNode(titulo[i]);
      celda.appendChild(textoCelda);
      hilera.appendChild(celda);
      }
    for (var i = 0; i <dato.length; i++) {
    var hilera2 = document.createElement("tr");
    var celdas = document.createElement("td");
    var textoCeldas = document.createTextNode(i+1);
    celdas.appendChild(textoCeldas);
      hilera2.appendChild(celdas);
    for (var j = 0; j < dato[i].length; j++) {
      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(dato[i][j]);
      celda.appendChild(textoCelda);
      hilera2.appendChild(celda);
    }
    tblBody.appendChild(hilera2);
    tdhead.appendChild(hilera);
  }
  tabla.appendChild(tdhead);
  tabla.appendChild(tblBody);
  body.appendChild(tabla);
  tabla.setAttribute("border", "2");
}


crear_tabla(titulox,datox);
